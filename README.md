Spirit::Press
--------------

头条插件 for [Spirit](https://getspirit.com) .

## Usage

## Installation

在 Spirit 应用的 Gemfile 增加:

```ruby
gem 'spirit-press'
```

然后执行 `bundle install`

## Configuration

修改 Spirit 的 `modules` 配置，增加 `press` 以启用。

```yml
defaults: &defaults
  # add "press" to modules
  modules: 'topic,...,press'
```

## Screenshot

![2017-02-28 10 52 08](https://cloud.githubusercontent.com/assets/5518/23389601/f72025f2-fda3-11e6-8340-7845c21e3a06.png)

## Contributing

如果你需要开发此插件，你需要同时准备好 Spirit 项目的开发环境，并修改 Spirit 项目 Gemfile:

```rb
gem 'spirit-press', path: '../spirit-press'
````

将 spirit-press 路径指向一个上层目录

```
~/work/spirit
~/work/spirit-press
```

然后执行 `bundle install` 和 `rails db:migrate`

现在，插件已经挂上了（别忘了开启 modules 的配置），然后在 Spirit 下面启动 `rails s`

访问 http://localhost:3000/posts 来验证功能。

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
