module Spirit
  module Press
    class Engine < ::Rails::Engine
      isolate_namespace Spirit::Press

      initializer 'spirit.press.init' do |app|
        break unless Setting.has_module?(:press)
        app.config.after_initialize do
          # Because need use I18n.t
          # must after Rails application initialized will get correct locale
          Spirit.register_plugin do |plugin|
            plugin.name              = 'press'
            plugin.display_name      = I18n.t('plugin.press')
            plugin.description       = 'A Press/News plugin for Spirit.'
            plugin.navbar_link       = true
            plugin.admin_navbar_link = true
            plugin.root_path         = "/posts"
            plugin.admin_path        = "/admin/posts"
          end
        end


        User.send :include, Spirit::Press::UserMixin

        app.routes.prepend do
          mount Spirit::Press::Engine => '/'
        end

        app.config.paths["db/migrate"].concat(config.paths["db/migrate"].expanded)
      end
    end
  end
end
