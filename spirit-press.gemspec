$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "spirit/press/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "spirit-press"
  s.version     = Spirit::Press::VERSION
  s.authors     = ["Jason Lee"]
  s.email       = ["powermedia@qq.com"]
  s.homepage    = "http://sucj.net"
  s.summary     = "Press/Blog/News plugin for Spirit."
  s.description = "Press/Blog/News plugin for Spirit, this gem is only work on Spirit Application."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5"
end
