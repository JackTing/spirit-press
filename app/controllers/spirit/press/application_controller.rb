module Spirit::Press
  class ApplicationController < ::ApplicationController
    helper Spirit::Press::ApplicationHelper

    def current_ability
      @current_ability ||= Spirit::Press::Ability.new(current_user)
    end
  end
end
